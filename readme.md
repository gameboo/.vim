#.vim with useful config#

Mostly following instructions from the github
[vim-pathogen](https://github.com/tpope/vim-pathogen.git) repo by
[tpope](http://tpo.pe/).

A default .vimrc.example (using pathogen) is available to copy in your home
directory. Plugins are added as git submodules when possible, that is:
```bash
$ cd ~/.vim/
$ git submodule add <path-to-plugin-git-repo> bundle/<plugin-name>
```
Here are the plugins that are imported :

* [vim-sensible](https://github.com/tpope/vim-sensible.git)
* [pyflakes](https://github.com/kevinw/pyflakes-vim.git)
* [vim-scala](https://github.com/derekwyatt/vim-scala)
* [vim-l3](https://github.com/gameboo/vim-l3.git)
* [vim-markdown](https://github.com/plasticboy/vim-markdown.git)
* [nerdtree](https://github.com/scrooloose/nerdtree.git)

After cloning this repository, to actually get the plugins, run:
```bash
$ cd ~/.vim/
$ git submodule update --init --recursive
```
